# Please replace the region and bucket names and 
# input output folders on s3 and ec2.key.name
# to match your setup
# upload.jar is the folder name of where you want to upload jar on s3
# upload.s3jarpath is the path of the jar used for step


# AWS S3 Initialization
region = us-west-2
check.bucket = s3://a7prediction
check.input = s3://a7prediction/a7history
check.logs = s3://a7prediction/logs
delete.output = s3://a7prediction/a7outputsa
upload.jar = s3://a7prediction
upload.s3jarpath = s3://a7prediction/a7_routing.jar

# AWS EMR Cluster Configuration
cluster.name = A7Cluster
step.name = Step
release.label = emr-4.2.0
log.uri = s3://a7prediction/logs
service.role = EMR_DefaultRole
job.flow.role = EMR_EC2_DefaultRole
ec2.key.name = subramanya.a_key_pair
instance.count = 10
keep.job.flow.alive = false
master.instance.type = m3.xlarge
slave.instance.type = m3.xlarge


# input directory  (required)
input = s3://a7prediction/a7history
# output directory (required)
output = s3://a7prediction/a7outputsa


