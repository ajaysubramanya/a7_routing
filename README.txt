################################################################################
# README-A7 ROUTING
# Smitha Bangalore Naresh, Prasad Memane, Swapnil Mahajan, Ajay Subramanya

# bangalorenaresh{dot}s{at}husky{dot}neu{dot}edu 
# memane{dot}p{at}husky{dot}neu{dot}edu
# mahajan{dot}sw{at}husky{dot}neu{dot}edu
# subramanya{dot}a{at}husky{dot}neu{dot}edu
################################################################################

################################  PREREQUISITES ################################

* Update perf.txt with your setup specific details. Instructions are present in
  perf.txt

* We are using Apache Maven 3.3.9 for dependency management and build automation

* Please `chmod +x runScript` if it is not already executable

* Sample output can be found in `sampleOutput` directory

* Report is named as a7Routing.pdf

* You need to have java installed on the machine you are running the script, 
  since we are performing some local computations on the output data to 
  generate the itineraries.

* We need around 6GB of "free RAM" on your local machine at the time of running 
  this.

* Copy `04missed.csv.gz` and `04req10k.csv.gz` to the project base directory.

* Update the region where you want to spawn the cluster.

################################## RUN SCRIPT ##################################

* `./runScript.sh` to run the script.

* You will be prompted to key in a few values we may need. Please enter then as
  required. Instructions are in the prompt. 

* The script is interactive and will tell you the about the progress.

* Total duration (with penalty) along with number of flights that were missed 
  from the proposed itinerary are logged to console.

* Once running on cloud is complete, the connections formed from the test data 
  will be download from your cloud output folder into a7_routing.csv and then 
  using a standalone java program we will propose routes for the requests from 
  the request file.

################################## CAVEATS #####################################

* Main class is RoutingDriver.java

* Project structure is as below,
                                                                   
.
├── README.txt
├── a7Routing.pdf
├── a7_itinerary
│   ├── pom.xml
│   └── src
│       ├── main
│       └── test
├── perf.txt
├── pom.xml
├── runScript
├── sampleOutput
│   └── Itinerary.txt
├── src
│   ├── main
│   │   └── java
│   └── test
│       └── java
└── uber-perf-0.0.1.jar

10 directories, 8 files

* Code to form itineraries from the predictions got from the cloud is present in 
  project base directory. 

* We are using the prof.'s code to upload to S3 and kick off the
  cluster[With minor changes]


################################### THE END ####################################
