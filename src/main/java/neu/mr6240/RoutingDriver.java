package neu.mr6240;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import neu.mr6240.mapreduce.model.ModelMapper;
import neu.mr6240.mapreduce.model.ModelReducer;
import neu.mr6240.mapreduce.prediction.PredictionMapper;
import neu.mr6240.mapreduce.prediction.PredictionReducer;
import neu.mr6240.mapreduce.test.TestMapper;
import neu.mr6240.mapreduce.test.TestReducer;
import neu.mr6240.mapreduce.train.TrainMapper;
import neu.mr6240.mapreduce.train.TrainReducer;
import neu.mr6240.utils.CLI;
import neu.mr6240.utils.CarrierOrgDep;
import neu.mr6240.utils.CompTestValue;
import neu.mr6240.utils.CompositeValue;
import neu.mr6240.utils.Utils;
import neu.mr6240.utils.mapreduce.PredictionModel;
import neu.mr6240.utils.mapreduce.TrainingModel;

/**
 * This is the main class which runs the mapreduce jobs
 *
 * @author prasadmemane
 * @author swapnilmahajan
 * @author smitha
 * @author ajay
 */
public class RoutingDriver extends Configured implements Tool {
	private static final String MODELS = "models";

	public static void main(String[] args) throws Exception {
		ToolRunner.run(new RoutingDriver(), args);
	}

	@Override
	public int run(String[] args) throws Exception {
		CLI cmd = new CLI(args);

		Configuration conf = getConf();
		conf.set("mapreduce.output.textoutputformat.separator", ",");

		Utils.sumbitJob(cmd.getTestDir(), cmd.getTestOpDir(), conf, TestMapper.class, TestReducer.class,
				CarrierOrgDep.class, CompTestValue.class);

		Utils.waitForJob(Utils.sumbitJob(cmd.getInputDir(), cmd.getInputOpDir(), conf, TrainMapper.class,
				TrainReducer.class, CarrierOrgDep.class, CompositeValue.class));

		Utils.waitForJob(Utils.sumbitJob(cmd.getInputOpDir(), MODELS, conf, ModelMapper.class, ModelReducer.class,
				Text.class, TrainingModel.class));

		Utils.waitForJob(Utils.sumbitJob(cmd.getTestOpDir(), cmd.getOutputDir(), conf, PredictionMapper.class,
				PredictionReducer.class, Text.class, PredictionModel.class));

		return 0;
	}
}
