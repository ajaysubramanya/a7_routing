package neu.mr6240.utils;

import org.joda.time.LocalDateTime;

import com.google.common.base.Strings;

/**
 * This is a class which is used for holding all the CRS_TIME and ACTUAL_TIME in
 * LocalDateTime format
 *
 * @author prasad memane
 * @author swapnil mahajan
 * @info - initial version for a5-paths
 * @author smitha
 * @author ajay
 * @info - updated version for a7-routing - added city and elapsedTime
 */
public class MapValue {

	private static final int ROLLOVER_DAY = 1;

	private LocalDateTime crsDateTime;
	private LocalDateTime actualDateTime;
	private int cancelled;

	private String city;
	private int elapsedTime;

	public MapValue(int year, int month, int dayOfMonth, String crsTime, String time, int cancelled, String oriCity,
			int elpTime) {
		this.crsDateTime = createDateTime(year, month, dayOfMonth, crsTime);
		this.actualDateTime = createDateTime(year, month, dayOfMonth, time);
		this.cancelled = cancelled;
		this.city = oriCity;
		this.elapsedTime = elpTime;
	}

	public MapValue(int year, int month, int dayOfMonth, String crsTime, String time, int cancelled, boolean rollOver,
			String desCity, int elpTime) {
		this.crsDateTime = createDateTime(year, month, dayOfMonth, crsTime).plusDays(ROLLOVER_DAY);
		this.actualDateTime = createDateTime(year, month, dayOfMonth, time).plusDays(ROLLOVER_DAY);
		this.cancelled = cancelled;
		this.city = desCity;
		this.elapsedTime = elpTime;
	}

	private LocalDateTime createDateTime(int year, int month, int dayOfMonth, String time) {
		String time1 = Strings.padStart(time, 4, '0');

		int hour = Integer.valueOf(time1.substring(0, 2));
		int min = Integer.valueOf(time1.substring(2, 4));

		// 2400 should be 0000
		if (hour == 24) hour = 0;

		return new LocalDateTime(year, month, dayOfMonth, hour, min);
	}

	public LocalDateTime getCrsDateTime() {
		return crsDateTime;
	}

	public void setCrsDateTime(LocalDateTime crsDateTime) {
		this.crsDateTime = crsDateTime;
	}

	public LocalDateTime getActualDateTime() {
		return actualDateTime;
	}

	public void setActualDateTime(LocalDateTime actualDateTime) {
		this.actualDateTime = actualDateTime;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getCancelled() {
		return cancelled;
	}

	public void setCancelled(int cancelled) {
		this.cancelled = cancelled;
	}

	public int getElapsedTime() {
		return elapsedTime;
	}

	public void setElapsedTime(int elapsedTime) {
		this.elapsedTime = elapsedTime;
	}
}
