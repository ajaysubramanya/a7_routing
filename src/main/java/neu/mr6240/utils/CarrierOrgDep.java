package neu.mr6240.utils;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import com.google.common.collect.ComparisonChain;

/**
 * This is class for custom key which is used output'd by the mapper
 * 
 * @author prasad memane
 * @author swapnil mahajan
 */
public class CarrierOrgDep implements WritableComparable<CarrierOrgDep> {

	private Text carrier;
	private Text orgDep;

	public CarrierOrgDep() {
		set(new Text(), new Text());
	}

	public CarrierOrgDep(String carrier, String orgDep) {
		set(new Text(carrier), new Text(orgDep));
	}

	public void set(Text carrier, Text orgDep) {
		this.carrier = carrier;
		this.orgDep = orgDep;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		carrier.readFields(in);
		orgDep.readFields(in);
	}

	@Override
	public void write(DataOutput out) throws IOException {
		carrier.write(out);
		orgDep.write(out);
	}

	@Override
	public int compareTo(CarrierOrgDep o) {
		return ComparisonChain.start().compare(carrier, o.carrier).compare(orgDep, o.orgDep).result();
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof CarrierOrgDep) {
			CarrierOrgDep c = (CarrierOrgDep) o;
			return carrier.equals(c.getCarrier()) && orgDep.equals(c.orgDep);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return carrier.hashCode() * 173 + orgDep.hashCode();
	}

	@Override
	public String toString() {
		return carrier + "\t" + orgDep + "\t";
	}

	public Text getCarrier() {
		return carrier;
	}

	public void setCarrier(Text carrier) {
		this.carrier = carrier;
	}

	public Text getOrgDep() {
		return orgDep;
	}

	public void setOrgDep(Text orgDep) {
		this.orgDep = orgDep;
	}

}
