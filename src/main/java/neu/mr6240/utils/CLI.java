package neu.mr6240.utils;

/**
 * This class gets the parses the parameters from the command line arguments
 *
 * @author prasad memane
 * @author swapnil mahjan
 */
public class CLI {
	public static final int INPUT_DIR_INDEX = 0;
	public static final int OUTPUT_DIR_INDEX = 1;
	public static final int INPUTOP_DIR_INDEX = 2;
	public static final int TEST_DIR_INDEX = 3;
	public static final int TESTOP_DIR_INDEX = 4;

	public static final String CMD_SEPERATOR = "=";

	private String[] args;

	private String inputDir;
	private String testDir;
	private String outputDir;
	private String inputOpDir;
	private String testOpDir;

	public String getInputOpDir() {
		return inputOpDir;
	}

	public void setInputOpDir(String inputOpDir) {
		this.inputOpDir = inputOpDir.substring(inputOpDir.indexOf(CMD_SEPERATOR) + 1);

	}

	public String getTestOpDir() {
		return testOpDir;
	}

	public void setTestOpDir(String outputOpDir) {
		this.testOpDir = outputOpDir.substring(outputOpDir.indexOf(CMD_SEPERATOR) + 1);

	}

	public CLI(String[] args) {
		this.args = args;
		setInputDir(args[INPUT_DIR_INDEX]);
		setInputOpDir(args[INPUTOP_DIR_INDEX]);
		setTestDir(args[TEST_DIR_INDEX]);
		setTestOpDir(args[TESTOP_DIR_INDEX]);
		setOutputDir(args[OUTPUT_DIR_INDEX]);
	}

	public String[] getArgs() {
		return args;
	}

	public void setArgs(String[] args) {
		this.args = args;
	}

	public String getInputDir() {
		return inputDir;
	}

	/**
	 * Set the argument for parameter "input="
	 *
	 * @param inputDir
	 */
	public void setInputDir(String inputDir) {
		this.inputDir = inputDir.substring(inputDir.indexOf(CMD_SEPERATOR) + 1);
	}

	public String getOutputDir() {
		return outputDir;
	}

	/**
	 * Set the argument for parameter "output="
	 *
	 * @param outputDir
	 */
	public void setOutputDir(String outputDir) {
		this.outputDir = outputDir.substring(outputDir.indexOf(CMD_SEPERATOR) + 1);
	}

	public String getTestDir() {
		return testDir;
	}

	public void setTestDir(String testDir) {
		this.testDir = testDir.substring(testDir.indexOf(CMD_SEPERATOR) + 1);
	}

}
