package neu.mr6240.utils;

import java.io.ByteArrayOutputStream;

/**
* Class that holds general utility functions
*
* @author Smitha Bangalore Naresh
* @author Ajay Subramanya
* @author Prasad Memane
* @author Swapnil Mahajan
*
*/

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.joda.time.LocalDateTime;

import com.google.common.base.Strings;

import neu.mr6240.RoutingDriver;
import weka.classifiers.Classifier;

public class Utils {
	/**
	 * submits the job application to the scheduler
	 *
	 * @param inputPath
	 *            the path where the input file is present
	 * @param outputPath
	 *            the path where the output should be written
	 * @param conf
	 *            Configurations
	 * @param map
	 *            the mapper class
	 * @param reduce
	 *            the reducer class
	 * @param outputKey
	 *            the output key class
	 * @param outputVal
	 *            the output value class
	 * @return the job
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Job sumbitJob(String inputPath, String outputPath, Configuration conf, Class map, Class reduce,
			Class outputKey, Class outputVal) throws IOException, InterruptedException, ClassNotFoundException {
		Job job = Job.getInstance(conf);
		job.setJarByClass(RoutingDriver.class);
		job.setOutputKeyClass(outputKey);
		job.setOutputValueClass(outputVal);
		FileInputFormat.addInputPath(job, new Path(inputPath));
		FileOutputFormat.setOutputPath(job, new Path(outputPath));
		job.setMapperClass(map);
		job.setReducerClass(reduce);
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		job.submit();
		return job;
	}

	/**
	 * waits for the submitted job to terminate
	 *
	 * @param job
	 *            the job to wait for
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void waitForJob(Job job) throws ClassNotFoundException, IOException, InterruptedException {
		if (!job.waitForCompletion(true)) {
			new Error("Aborted due to failure of job1");
			System.exit(-1);
		}
	}

	/**
	 *
	 * @param obj
	 *            the classifier object which we need to write to file on HDFS
	 * @param name
	 *            the name of the file in HDFS
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public static void writeFile(Classifier obj, String name) throws IOException, URISyntaxException {
		FileSystem hdfs = FileSystem.get(new Configuration());
		Path path = new Path(name);
		if (hdfs.exists(path)) hdfs.delete(path, true);
		OutputStream os = hdfs.create(path);
		os.write(serialize(obj));
		hdfs.close();
	}

	/**
	 *
	 * @param name
	 *            the name of the file we want to read
	 * @return the classifier object
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static Classifier readFile(String name) throws IOException, ClassNotFoundException {
		FileSystem fs = FileSystem.get(new Configuration());
		InputStream in = fs.open(new Path(name));
		ObjectInputStream objReader = new ObjectInputStream(in);
		return (Classifier) objReader.readObject();
	}

	/**
	 *
	 * @param obj
	 *            an object to serialise
	 * @return a serialised object
	 * @throws IOException
	 */
	public static byte[] serialize(Object obj) throws IOException {
		try (ByteArrayOutputStream b = new ByteArrayOutputStream()) {
			try (ObjectOutputStream o = new ObjectOutputStream(b)) {
				o.writeObject(obj);
			}
			return b.toByteArray();
		}
	}

	/**
	 *
	 * @param airport
	 * @return the value of airport from "neu.mr.cs6240.a6_prediction.Airports"
	 *         , if not present then OTHER
	 */
	public static String getAirports(String airport) {
		Airports dest = Airports.OTHER;
		try {
			dest = Airports.valueOf(airport);
		} catch (IllegalArgumentException ex) {
			dest = Airports.OTHER;
		}
		return dest.toString();
	}

	/**
	 *
	 * @param airport
	 * @return the value of airport from "neu.mr.cs6240.a6_prediction.Airports"
	 *         , if not present then OTHER
	 */
	public static String getOriginAirports(String airport) {
		OriginAirports dest = OriginAirports.OTHER;
		try {
			dest = OriginAirports.valueOf(airport);
		} catch (IllegalArgumentException ex) {
			dest = OriginAirports.OTHER;
		}
		return dest.toString();
	}

	/**
	 *
	 * @param airport
	 * @return the value of airport from "neu.mr.cs6240.a6_prediction.Airports"
	 *         , if not present then OTHER
	 */
	public static String getDestAirports(String airport) {
		DestAirports dest = DestAirports.OTHER;
		try {
			dest = DestAirports.valueOf(airport);
		} catch (IllegalArgumentException ex) {
			dest = DestAirports.OTHER;
		}
		return dest.toString();
	}

	/**
	 *
	 * @param carrier
	 * @return the value of the carrier from
	 *         "neu.mr.cs6240.a6_prediction.Carriers" , if not present then
	 *         OTHER
	 */
	public static String getCarrier(String carrier) {
		Carriers dest = Carriers.OTHER;
		try {
			dest = Carriers.valueOf(carrier);
		} catch (IllegalArgumentException ex) {
			dest = Carriers.OTHER;
		}
		return dest.toString();
	}

	/**
	 * This method checks if the arrival day is the next day of the departure
	 * day
	 *
	 * @param year
	 * @param month
	 * @param dayOfMonth
	 * @param arrTime1
	 * @param depTime1
	 * @param elapsedTime
	 * @return true if the arrival day is the next day of the departure day,
	 *         else false
	 */
	public static boolean checkDayRollOver(String year, String month, String dayOfMonth, String arrTime1,
			String depTime1, String elapsedTime) {
		String depTime = Strings.padStart(depTime1, 4, '0');
		String arrTime = Strings.padStart(arrTime1, 4, '0');

		int deptHour = Integer.valueOf(depTime.substring(0, 2));
		int arrHour = Integer.valueOf(arrTime.substring(0, 2));
		int deptMin = Integer.valueOf(depTime.substring(2, 4));
		int arrMin = Integer.valueOf(arrTime.substring(2, 4));

		// 2400 should be 0000
		if (deptHour == 24) deptHour = 0;
		if (arrHour == 24) arrHour = 0;

		LocalDateTime arrDateTime = new LocalDateTime(Integer.valueOf(year), Integer.valueOf(month),
				Integer.valueOf(dayOfMonth), arrHour, arrMin);
		LocalDateTime deptDateTime = new LocalDateTime(Integer.valueOf(year), Integer.valueOf(month),
				Integer.valueOf(dayOfMonth), deptHour, deptMin);

		deptDateTime.plusMinutes(Integer.valueOf(elapsedTime));

		return deptDateTime.toLocalDate().equals(arrDateTime.toLocalDate());
	}
}
