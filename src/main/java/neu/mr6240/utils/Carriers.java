package neu.mr6240.utils;

/**
 * Carriers picked after running a MR job and finding the airports with
 * most flights. These were the only airports we found in the provided dataset
 *
 * @author Smitha Bangalore Naresh
 * @author Ajay Subramanya
 *
 */

public enum Carriers {

		AA(1),
		AS(2),
		B6(3),
		CO(4),
		DH(5),
		DL(6),
		EV(7),
		F9(8),
		FL(9),
		HA(10),
		HP(11),
		MQ(12),
		NW(13),
		OH(14),
		OO(15),
		RU(16),
		TZ(17),
		UA(18),
		US(19),
		WN(20),
		OTHER(21);

	private int value;

	private Carriers(int value) {
		this.value = value;
	}

	public int val() {
		return value;
	}
};
