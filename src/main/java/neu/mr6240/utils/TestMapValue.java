package neu.mr6240.utils;

import org.joda.time.LocalDateTime;

import com.google.common.base.Strings;

/**
 * This is a class which is used for holding all the CRS_TIME and ACTUAL_TIME in
 * LocalDateTime format
 *
 * @author prasadmemane
 * @author swapnilmahajan
 * @info - inital version for a5-paths
 * @author smitha
 * @info - updated version for a7-routing - added city and elapsedTime
 */
public class TestMapValue {

	private static final int ROLLOVER_DAY = 1;

	private LocalDateTime crsDateTime;
	private int flightNo;

	private String city;
	private int elapsedTime;

	public TestMapValue(int year, int month, int dayOfMonth, String crsTime, int flightNo, String oriCity,
			int elpTime) {
		this.crsDateTime = createDateTime(year, month, dayOfMonth, crsTime);
		this.flightNo=flightNo;
		this.city = oriCity;
		this.elapsedTime = elpTime;
	}

	public TestMapValue(int year, int month, int dayOfMonth, String crsTime, int flightNo, boolean rollOver,
			String desCity, int elpTime) {
		this.crsDateTime = createDateTime(year, month, dayOfMonth, crsTime).plusDays(ROLLOVER_DAY);
		this.flightNo=flightNo;
		this.city = desCity;
		this.elapsedTime = elpTime;
	}

	private LocalDateTime createDateTime(int year, int month, int dayOfMonth, String time) {
		String time1 = Strings.padStart(time, 4, '0');

		int hour = Integer.valueOf(time1.substring(0, 2));
		int min = Integer.valueOf(time1.substring(2, 4));

		// 2400 should be 0000
		if (hour == 24) hour = 0;

		return new LocalDateTime(year, month, dayOfMonth, hour, min);
	}

	public LocalDateTime getCrsDateTime() {
		return crsDateTime;
	}

	public void setCrsDateTime(LocalDateTime crsDateTime) {
		this.crsDateTime = crsDateTime;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(int flightNo) {
		this.flightNo = flightNo;
	}

	public int getElapsedTime() {
		return elapsedTime;
	}

	public void setElapsedTime(int elapsedTime) {
		this.elapsedTime = elapsedTime;
	}
}
