package neu.mr6240.utils.mapreduce;

/**
 * indices of the model
 * 
 * @author ajay subramanya
 * @author smitha bangalore naresh
 * @author prasad memane
 * @author swapnil mahajan
 * 
 */
public enum PredictionIndex {

	CARRIER(0), ORIGIN(1), CONN(2), DEST(3), YEAR(4), MONTH(5), DAY(6), ARR_HR(7), DEP_HR(8), DURATION(9), ORG_CONNECT(
			10), CONNECT_DEST(11);

	private int value;

	private PredictionIndex(int value) {
		this.value = value;
	}

	public int val() {
		return value;
	}

}
