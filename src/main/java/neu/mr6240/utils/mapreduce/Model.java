package neu.mr6240.utils.mapreduce;

/**
 * @author ajay subramanya
 * @author smitha bangalore naresh 
 * @author prasad memane
 * @author swapnil mahajan
 * 
 * the base class for a model, mainly contains all the data needed
 * to build a model.
 * 
 */

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.ShortWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

public class Model implements Writable {
	private Text carrier;
	private Text origin;
	private Text dest;
	private IntWritable year;
	private ShortWritable month;
	private ShortWritable day;
	private ShortWritable arr;
	private ShortWritable dep;
	private IntWritable duration;

	/**
	 * default constructor
	 */
	public Model() {
		this(new Text(), new Text(), new Text(), new IntWritable(), new ShortWritable(), new ShortWritable(),
				new ShortWritable(), new ShortWritable(), new IntWritable());
	}

	/**
	 * Parameterised constructor
	 */
	public Model(Text carrier, Text origin, Text dest, IntWritable year, ShortWritable month, ShortWritable day,
			ShortWritable arr, ShortWritable dep, IntWritable duration) {
		this.carrier = carrier;
		this.origin = origin;
		this.dest = dest;
		this.year = year;
		this.month = month;
		this.day = day;
		this.arr = arr;
		this.dep = dep;
		this.duration = duration;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		carrier.write(out);
		origin.write(out);
		dest.write(out);
		year.write(out);
		month.write(out);
		day.write(out);
		arr.write(out);
		dep.write(out);
		duration.write(out);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		carrier.readFields(in);
		origin.readFields(in);
		dest.readFields(in);
		year.readFields(in);
		month.readFields(in);
		day.readFields(in);
		arr.readFields(in);
		dep.readFields(in);
		duration.readFields(in);
	}

	@Override
	public String toString() {
		return "Model [carrier=" + carrier + ", origin=" + origin + ", dest=" + dest + ", year=" + year + ", month="
				+ month + ", day=" + day + ", arr=" + arr + ", dep=" + dep + ", duration=" + duration + "]";
	}

	public Text getCarrier() {
		return carrier;
	}

	public void setCarrier(Text carrier) {
		this.carrier = carrier;
	}

	public Text getOrigin() {
		return origin;
	}

	public void setOrigin(Text origin) {
		this.origin = origin;
	}

	public Text getDest() {
		return dest;
	}

	public void setDest(Text dest) {
		this.dest = dest;
	}

	public IntWritable getYear() {
		return year;
	}

	public void setYear(IntWritable year) {
		this.year = year;
	}

	public ShortWritable getMonth() {
		return month;
	}

	public void setMonth(ShortWritable month) {
		this.month = month;
	}

	public ShortWritable getDay() {
		return day;
	}

	public void setDay(ShortWritable day) {
		this.day = day;
	}

	public ShortWritable getArr() {
		return arr;
	}

	public void setArr(ShortWritable arr) {
		this.arr = arr;
	}

	public ShortWritable getDep() {
		return dep;
	}

	public void setDep(ShortWritable dep) {
		this.dep = dep;
	}

	public IntWritable getDuration() {
		return duration;
	}

	public void setDuration(IntWritable duration) {
		this.duration = duration;
	}
}
