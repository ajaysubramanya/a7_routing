package neu.mr6240.utils.mapreduce;

/**
 * fields used for training the model 
 * 
 * @author ajay subramanya
 * @author smitha bangalore
 * @author prasad memane
 * @author swapnil mahajan
 */

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.ShortWritable;
import org.apache.hadoop.io.Text;

public class TrainingModel extends Model {

	private BooleanWritable missed;

	/**
	 * default constructor
	 */
	public TrainingModel() {
		this(new Text(), new Text(), new Text(), new IntWritable(), new ShortWritable(), new ShortWritable(),
				new ShortWritable(), new ShortWritable(), new IntWritable(), new BooleanWritable());
	}

	/**
	 * parametrised constructor
	 * 
	 * @param carrier
	 * @param origin
	 * @param dest
	 * @param year
	 * @param month
	 * @param day
	 * @param arr
	 * @param dep
	 * @param duration
	 * @param missed
	 */
	public TrainingModel(Text carrier, Text origin, Text dest, IntWritable year, ShortWritable month, ShortWritable day,
			ShortWritable arr, ShortWritable dep, IntWritable duration, BooleanWritable missed) {
		super(carrier, origin, dest, year, month, day, arr, dep, duration);
		this.missed = missed;
	}

	/**
	 * 
	 * @param val
	 *            string array that has the data from the input file
	 * @return Model
	 */
	public static TrainingModel set(String[] val) {
		return new TrainingModel(new Text(val[TrainingIndex.CARRIER.val()]), new Text(val[TrainingIndex.ORIGIN.val()]),
				new Text(val[TrainingIndex.DEST.val()]),
				new IntWritable(Integer.parseInt(val[TrainingIndex.YEAR.val()])),
				new ShortWritable(Short.parseShort(val[TrainingIndex.MONTH.val()])),
				new ShortWritable(Short.parseShort(val[TrainingIndex.DAY.val()])),
				new ShortWritable(Short.parseShort(val[TrainingIndex.ARR_HR.val()])),
				new ShortWritable(Short.parseShort(val[TrainingIndex.DEP_HR.val()])),
				new IntWritable(Integer.parseInt(val[TrainingIndex.DURATION.val()])),
				new BooleanWritable(Boolean.parseBoolean(val[TrainingIndex.MISSED.val()])));
	}

	@Override
	public void write(DataOutput out) throws IOException {
		super.write(out);
		missed.write(out);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		super.readFields(in);
		missed.readFields(in);
	}

	public BooleanWritable getMissed() {
		return missed;
	}

	public void setMissed(BooleanWritable missed) {
		this.missed = missed;
	}

}
