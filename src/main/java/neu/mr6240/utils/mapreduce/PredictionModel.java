package neu.mr6240.utils.mapreduce;

/**
 * Extends Model, used for prediction. Includes two new fields needed 
 * for prediction - origin flight and destination flight  
 * 
 * @author ajay subramanya
 * @author smitha bangalore naresh
 * @author prasad memane
 * @author swapnil mahajan
 */

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.ShortWritable;
import org.apache.hadoop.io.Text;

public class PredictionModel extends Model {
	private ShortWritable orgConnect;
	private ShortWritable connectDest;

	public PredictionModel() {
		this(new Text(), new Text(), new Text(), new IntWritable(), new ShortWritable(), new ShortWritable(),
				new ShortWritable(), new ShortWritable(), new IntWritable(), new ShortWritable(), new ShortWritable());
	}

	public PredictionModel(Text carrier, Text origin, Text dest, IntWritable year, ShortWritable month,
			ShortWritable day, ShortWritable arr, ShortWritable dep, IntWritable duration, ShortWritable orgConnect,
			ShortWritable connectDest) {
		super(carrier, origin, dest, year, month, day, arr, dep, duration);
		this.orgConnect = orgConnect;
		this.connectDest = connectDest;
	}

	/**
	 * 
	 * @param val
	 *            string array that has the data from the input file
	 * @return Model
	 */
	public static PredictionModel set(String[] val) {
		return new PredictionModel(new Text(val[PredictionIndex.CARRIER.val()]),
				new Text(val[PredictionIndex.ORIGIN.val()]), new Text(val[PredictionIndex.DEST.val()]),
				new IntWritable(Integer.parseInt(val[PredictionIndex.YEAR.val()])),
				new ShortWritable(Short.parseShort(val[PredictionIndex.MONTH.val()])),
				new ShortWritable(Short.parseShort(val[PredictionIndex.DAY.val()])),
				new ShortWritable(Short.parseShort(val[PredictionIndex.ARR_HR.val()])),
				new ShortWritable(Short.parseShort(val[PredictionIndex.DEP_HR.val()])),
				new IntWritable(Integer.parseInt(val[PredictionIndex.DURATION.val()])),
				new ShortWritable(Short.parseShort(val[PredictionIndex.ORG_CONNECT.val()])),
				new ShortWritable(Short.parseShort(val[PredictionIndex.CONNECT_DEST.val()])));
	}

	@Override
	public void write(DataOutput out) throws IOException {
		super.write(out);
		orgConnect.write(out);
		connectDest.write(out);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		super.readFields(in);
		orgConnect.readFields(in);
		connectDest.readFields(in);
	}

	public ShortWritable getOrgConnect() {
		return orgConnect;
	}

	public void setOrgConnect(ShortWritable orgConnect) {
		this.orgConnect = orgConnect;
	}

	public ShortWritable getConnectDest() {
		return connectDest;
	}

	public void setConnectDest(ShortWritable connectDest) {
		this.connectDest = connectDest;
	}

}
