package neu.mr6240.utils;

public class Constants {
	// MAPPER CONSTANTS
	public static final boolean DEFAULT_ROLL_OVER_FLAG = false;

	public static final int ORIGIN_INDEX = 0;
	public static final int DESTINATION_INDEX = 1;
	public static final int HISTORY_COLUMNS = 110;

	public static final int CANCELLED = 47;
	public static final int CARRIER = 6;
	public static final int YEAR = 0;
	public static final int MONTH = 2;
	public static final int DAY_OF_MONTH = 3;

	public static final int CRS_ARR_TIME = 40;
	public static final int CRS_DEP_TIME = 29;
	public static final int CRS_ELAPSED_TIME = 50;
	public static final int ARR_TIME = 41;
	public static final int DEP_TIME = 30;

	public static final int ORIGIN = 14;
	public static final int DESTINATION = 23;
	public static final int FLIGHTNO = 10;
	public static final int TEST_COLUMNS = 111;

	//REDUCER CONSTANTS
	public static final int ONE_HOUR_IN_MINUTES = 60;
	public static final short ONE_HOUR = 1;
	public static final short THIRTY_MINUTES = 30;
	public static final short _24_HOURS = 24;
	public static final short _0_HOURS = 0;

	public static final int CANCELLED_CODE = 1;

}
