package neu.mr6240.utils.weka;

/**
 * weka specific attributes and methods
 * 
 * @author ajay subramanya
 * @author smitha bangalore
 * @author prasad memane
 * @author swapnil mahajan
 */

import java.util.ArrayList;
import java.util.List;

import neu.mr6240.utils.Airports;
import neu.mr6240.utils.Carriers;
import weka.core.Attribute;

public class Weka {

	public static final short NUM_FEATURES = 9;
	public static final short MISSED_FEATURE_INDEX = 8;

	/**
	 * @return an arrayList of attributes
	 */
	public static ArrayList<Attribute> getAttrs() {
		ArrayList<Attribute> attr = new ArrayList<>();

		attr.add(new Attribute("CARRIER", getcarrier()));
		attr.add(new Attribute("ORIGIN", getAirports()));
		attr.add(new Attribute("DEST", getAirports()));
		attr.add(new Attribute("MONTH", Attribute.NUMERIC));
		attr.add(new Attribute("DAY", Attribute.NUMERIC));
		attr.add(new Attribute("ARR", Attribute.NUMERIC));
		attr.add(new Attribute("DEP", Attribute.NUMERIC));
		attr.add(new Attribute("DURATION", Attribute.NUMERIC));
		attr.add(new Attribute("MISSED", getMissed()));

		return attr;
	}

	/**
	 *
	 * @return a list of carrier values from the Enum
	 *
	 */
	public static List<String> getcarrier() {
		List<String> carrier = new ArrayList<>();
		for (Carriers i : Carriers.values()) {
			carrier.add(i.toString());
		}
		return carrier;
	}

	/**
	 *
	 * @return a list of airport values from
	 *
	 */
	public static List<String> getAirports() {
		List<String> airports = new ArrayList<>();
		for (Airports i : Airports.values()) {
			airports.add(i.toString());
		}
		return airports;
	}

	/**
	 *
	 * @return a list of missed value
	 */
	public static List<String> getMissed() {
		List<String> missed = new ArrayList<>();
		missed.add("false");
		missed.add("true");
		return missed;
	}
}
