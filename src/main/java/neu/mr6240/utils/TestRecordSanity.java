package neu.mr6240.utils;

/**
 * This class is used for the sanity check of the test record
 * @author prasadmemane
 * @author swapnilmahajan
 */
public class TestRecordSanity {
	private static final int YEAR=0;
	private static final int QUARTER=1;
	private static final int MONTH=2;
	private static final int DAY_OF_MONTH=3;
	private static final int DAY_OF_WEEK=4;
	private static final int CARRIER=6;
	private static final int FLIGHTNO=10;
	private static final int ORIGIN_AIRPORT_ID = 11; 
	private static final int ORIGIN_AIRPORT_SEQ_ID = 12; 
	private static final int ORIGIN_CITY_MARKET_ID = 13;
	private static final int ORIGIN = 14;
	private static final int ORIGIN_CITY_NAME = 15;
	private static final int ORIGIN_STATE_ABR = 16;
	private static final int ORIGIN_STATE_FIPS = 17;
	private static final int ORIGIN_STATE_NM = 18;
	private static final int ORIGIN_WAC = 19; 
	private static final int DEST_AIRPORT_ID = 20; 
	private static final int DEST_AIRPORT_SEQ_ID = 21; 
	private static final int DEST_CITY_MARKET_ID = 22;
	private static final int DEST = 23;
	private static final int DEST_CITY_NAME = 24;
	private static final int DEST_STATE_FIPS = 26; 
	private static final int DEST_WAC = 28;
	private static final int CRS_DEP_TIME = 29; 
	private static final int CRS_ARR_TIME = 40;
	private static final int CRS_ELAPSED_TIME = 50;
	private static final int DISTANCE = 54;
	 
	private static final String NA = "NA";
	
	
	/**
	 * This method checks if the record passes all the sanity tests
	 * @param record
	 * @return boolean This returns true if the flight/record fails the sanity test
	 */
	public boolean sanityFails(String[] record) {
		return(invalidFlightNo(record[FLIGHTNO]) ||
				checkNA(record) ||
				checkCRSARRAndDEPTime(record) ||
				checkOriginAndDestForZero(record) ||
				checkOriginAndDestForEmpty(record));
	}
	
	private boolean invalidFlightNo(String flightNo){
		return !flightNo.matches("[0-9]+");
	}
	
	/**
	 * Checks if any of the CRS_ARR_TIME, CRS_DEP_TIME,  fields contain
	 * NA value
	 * @param record
	 * @return
	 */
	private boolean checkNA(String[] record){
		return (record[CRS_ARR_TIME].equals(NA) || record[CRS_DEP_TIME].equals(NA) 
				|| record[YEAR].equals(NA) || record[QUARTER].equals(NA)
				|| record[MONTH].equals(NA) || record[DAY_OF_MONTH].equals(NA)
				|| record[DAY_OF_WEEK].equals(NA) || record[CARRIER].equals(NA)
				|| record[FLIGHTNO].equals(NA) || record[CRS_ELAPSED_TIME].equals(NA)
				|| record[DISTANCE].equals(NA));
	}
	
	/**
	 * Sanity test for: CRSArrTime and CRSDepTime should not be zero
	 * @param record
	 * @return boolean This returns true if CRSArrTime and CRSDepTime is zero
	 */
	private boolean checkCRSARRAndDEPTime(String[] record) {
		try{
		return (Integer.parseInt(record[CRS_ARR_TIME]) == 0 || 
				Integer.parseInt(record[CRS_DEP_TIME]) == 0);
		}catch (NumberFormatException e){
			return true;
		}
	}
	
	/**
	 * Sanity test for: AirportID,  AirportSeqID, CityMarketID, StateFips, Wac should be larger than 0
	 * @param record
	 * @return boolean This returns true if AirportID,  AirportSeqID, CityMarketID, StateFips, Wac is lesser than or equal to 0
	 */
	private boolean checkOriginAndDestForZero(String[] record) {
		return (Integer.parseInt(record[ORIGIN_AIRPORT_ID]) <= 0 || 
				Integer.parseInt(record[ORIGIN_AIRPORT_SEQ_ID]) <= 0 || 
				Integer.parseInt(record[ORIGIN_CITY_MARKET_ID]) <= 0 || 
				Integer.parseInt(record[ORIGIN_STATE_FIPS]) <= 0 ||
				Integer.parseInt(record[ORIGIN_WAC]) <= 0 || 
				Integer.parseInt(record[DEST_AIRPORT_ID]) <= 0 || 
				Integer.parseInt(record[DEST_AIRPORT_SEQ_ID]) <= 0 || 
				Integer.parseInt(record[DEST_CITY_MARKET_ID]) <= 0 || 
				Integer.parseInt(record[DEST_STATE_FIPS]) <= 0 ||
				Integer.parseInt(record[DEST_WAC]) <= 0);
	}
	
	/**
	 * Sanity test for: Origin, Destination,  CityName, State, StateName should not be empty
	 * @param record
	 * @return boolean This returns true if Origin, Destination,  CityName, State, StateName is empty
	 */
	private boolean checkOriginAndDestForEmpty(String[] record) {
		return (record[ORIGIN] == null || record[ORIGIN].isEmpty() || 
				record[DEST] == null || record[DEST].isEmpty() ||
				record[ORIGIN_CITY_NAME] == null || record[ORIGIN_CITY_NAME].isEmpty() ||
				record[DEST_CITY_NAME] == null || record[DEST_CITY_NAME].isEmpty() ||
				record[ORIGIN_STATE_NM] == null || record[ORIGIN_STATE_NM].isEmpty() || 
				record[ORIGIN_STATE_ABR] == null || record[ORIGIN_STATE_ABR].isEmpty());
	}
}
