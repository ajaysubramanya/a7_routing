package neu.mr6240.utils;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

/**
 * This is a class for the Value which is the output of the mapper and input to
 * the reducer
 *
 * @author prasad memane
 * @author swapnil mahajan
 * @author smitha
 */
public class CompositeValue implements Writable {

	private IntWritable year;
	private IntWritable month;
	private IntWritable dayOfMonth;

	private Text crsTime;
	private Text time;

	private IntWritable cancelled;

	/**
	 * This is a flag for if the Object holds origin fields or departure fields
	 * If index = 0, the object will hold origin, CRS_DEP_TIME, DEP_TIME. If
	 * index = 1, the object will hold destination, CRS_ARR_TIME, ARR_TIME
	 */
	private IntWritable index;

	/**
	 * This is a flag for rollOver, if the arrival day is the next day of the
	 * departure day
	 */
	private BooleanWritable rollOver;

	/**
	 * This maintains the city origin/destination flight If index = 0, the
	 * object will hold , destination If index = 1, the object will hold origin
	 */
	private Text city;

	/**
	 * This field is used to get the duration time
	 */
	private IntWritable elapsedTime;

	public CompositeValue() {
		set(new IntWritable(), new IntWritable(), new IntWritable(), new Text(), new Text(), new IntWritable(),
				new IntWritable(), new BooleanWritable(), new Text(), new IntWritable());
	}

	public CompositeValue(String year, String month, String dayOfMonth, String crsTime, String time, String cancelled,
			int index, boolean rollOver, String city, String elpTime) {
		set(new IntWritable(Integer.valueOf(year)), new IntWritable(Integer.valueOf(month)),
				new IntWritable(Integer.valueOf(dayOfMonth)), new Text(crsTime), new Text(time),
				new IntWritable(Integer.valueOf(cancelled)), new IntWritable(index), new BooleanWritable(rollOver),
				new Text(city), new IntWritable(Integer.valueOf(elpTime)));
	}

	private void set(IntWritable year, IntWritable month, IntWritable dayOfMonth, Text crsTime, Text time,
			IntWritable cancelled, IntWritable index, BooleanWritable rollOver, Text city, IntWritable elpTime) {
		this.year = year;
		this.month = month;
		this.dayOfMonth = dayOfMonth;
		this.crsTime = crsTime;
		this.time = time;
		this.cancelled = cancelled;
		this.index = index;
		this.rollOver = rollOver;
		this.city = city;
		this.elapsedTime = elpTime;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		year.readFields(in);
		month.readFields(in);
		dayOfMonth.readFields(in);
		crsTime.readFields(in);
		time.readFields(in);
		cancelled.readFields(in);
		index.readFields(in);
		rollOver.readFields(in);
		city.readFields(in);
		elapsedTime.readFields(in);
	}

	@Override
	public void write(DataOutput out) throws IOException {
		year.write(out);
		month.write(out);
		dayOfMonth.write(out);
		crsTime.write(out);
		time.write(out);
		cancelled.write(out);
		index.write(out);
		rollOver.write(out);
		city.write(out);
		elapsedTime.write(out);
	}

	@Override
	public String toString() {
		return year + "\t" + month + "\t" + dayOfMonth + "\t" + crsTime + "\t" + time + "\t" + cancelled + "\t" + index
				+ "\t" + rollOver + "\t" + city + "\t" + elapsedTime;
	}

	public IntWritable getYear() {
		return year;
	}

	public void setYear(IntWritable year) {
		this.year = year;
	}

	public IntWritable getMonth() {
		return month;
	}

	public void setMonth(IntWritable month) {
		this.month = month;
	}

	public IntWritable getDayOfMonth() {
		return dayOfMonth;
	}

	public void setDayOfMonth(IntWritable dayOfMonth) {
		this.dayOfMonth = dayOfMonth;
	}

	public Text getCrsTime() {
		return crsTime;
	}

	public void setCrsTime(Text crsTime) {
		this.crsTime = crsTime;
	}

	public Text getTime() {
		return time;
	}

	public void setTime(Text time) {
		this.time = time;
	}

	public IntWritable getCancelled() {
		return cancelled;
	}

	public void setCancelled(IntWritable cancelled) {
		this.cancelled = cancelled;
	}

	public IntWritable getIndex() {
		return index;
	}

	public void setIndex(IntWritable index) {
		this.index = index;
	}

	public BooleanWritable getRollOver() {
		return rollOver;
	}

	public void setRollOver(BooleanWritable rollOver) {
		this.rollOver = rollOver;
	}

	public Text getCity() {
		return city;
	}

	public void setCity(Text city) {
		this.city = city;
	}

	public IntWritable getElapsedTime() {
		return elapsedTime;
	}

	public void setElapsedTime(IntWritable elapsedTime) {
		this.elapsedTime = elapsedTime;
	}
}
