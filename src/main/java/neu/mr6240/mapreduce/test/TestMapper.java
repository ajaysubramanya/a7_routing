package neu.mr6240.mapreduce.test;

import static neu.mr6240.utils.Constants.CARRIER;
import static neu.mr6240.utils.Constants.CRS_ARR_TIME;
import static neu.mr6240.utils.Constants.CRS_DEP_TIME;
import static neu.mr6240.utils.Constants.CRS_ELAPSED_TIME;
import static neu.mr6240.utils.Constants.DAY_OF_MONTH;
import static neu.mr6240.utils.Constants.DEFAULT_ROLL_OVER_FLAG;
import static neu.mr6240.utils.Constants.DESTINATION;
import static neu.mr6240.utils.Constants.DESTINATION_INDEX;
import static neu.mr6240.utils.Constants.FLIGHTNO;
import static neu.mr6240.utils.Constants.MONTH;
import static neu.mr6240.utils.Constants.ORIGIN;
import static neu.mr6240.utils.Constants.ORIGIN_INDEX;
import static neu.mr6240.utils.Constants.TEST_COLUMNS;
import static neu.mr6240.utils.Constants.YEAR;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import au.com.bytecode.opencsv.CSVParser;
import neu.mr6240.utils.CarrierOrgDep;
import neu.mr6240.utils.CompTestValue;
import neu.mr6240.utils.TestRecordSanity;
import neu.mr6240.utils.Utils;

/**
 * This is the Mapper class for reading the test data
 *
 * @author prasad memane
 * @author swapnil mahajan
 * @author ajay subramanya
 * @author smitha bangalore naresh
 */
public class TestMapper extends Mapper<LongWritable, Text, CarrierOrgDep, CompTestValue> {

	private CSVParser parser = new CSVParser();
	private TestRecordSanity sanity = new TestRecordSanity();

	/**
	 * This method maps each row to a specific <Key,Value> which is the input to
	 * the reducer depending on the key It reads the input line by line and
	 * throws away the unnecessary information while generating the <Key, Value>
	 * pair
	 */
	@Override
	protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		String[] fields = parser.parseLine(value.toString());

		if (!value.toString().contains("YEAR") && fields.length == TEST_COLUMNS) {
			if (!sanity.sanityFails(fields)) {
				boolean rollOverDate = true;
				if (Utils.checkDayRollOver(fields[YEAR], fields[MONTH], fields[DAY_OF_MONTH], fields[CRS_ARR_TIME],
						fields[CRS_DEP_TIME], fields[CRS_ELAPSED_TIME]))
					rollOverDate = false;

				// Origin
				context.write(new CarrierOrgDep(fields[CARRIER], fields[ORIGIN]),
						new CompTestValue(fields[YEAR], fields[MONTH], fields[DAY_OF_MONTH], fields[CRS_DEP_TIME],
								fields[FLIGHTNO], ORIGIN_INDEX, rollOverDate, fields[DESTINATION],
								fields[CRS_ELAPSED_TIME]));

				// Destination
				context.write(new CarrierOrgDep(fields[CARRIER], fields[DESTINATION]),
						new CompTestValue(fields[YEAR], fields[MONTH], fields[DAY_OF_MONTH], fields[CRS_ARR_TIME],
								fields[FLIGHTNO], DESTINATION_INDEX, DEFAULT_ROLL_OVER_FLAG, fields[ORIGIN],
								fields[CRS_ELAPSED_TIME]));
			}
		}
	}
}
