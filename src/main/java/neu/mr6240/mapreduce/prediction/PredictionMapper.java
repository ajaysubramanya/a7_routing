package neu.mr6240.mapreduce.prediction;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import au.com.bytecode.opencsv.CSVParser;
import neu.mr6240.utils.mapreduce.Model;
import neu.mr6240.utils.mapreduce.PredictionModel;
import neu.mr6240.utils.mapreduce.TrainingIndex;

/**
 * This is the Mapper class for reading the test connections data for
 * predictions. Connections data contains missed info for a flight in test data.
 *
 * @author smitha bangalore
 * @author ajay subramanya
 * @author prasad memane
 * @author swapnil mahajan
 */
public class PredictionMapper extends Mapper<LongWritable, Text, Text, Model> {

	private CSVParser parser = new CSVParser();

	@Override
	protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		String[] fields = parser.parseLine(value.toString());
		context.write(new Text(fields[TrainingIndex.CONN.val()]), PredictionModel.set(fields));
	}
}
