package neu.mr6240.mapreduce.prediction;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import neu.mr6240.utils.Utils;
import neu.mr6240.utils.mapreduce.Model;
import neu.mr6240.utils.mapreduce.PredictionModel;
import neu.mr6240.utils.weka.Weka;
import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

/**
 * This is the Reducer class for predicting based on the model built if a
 * connection has a missed flight.
 *
 * @author smitha
 * @author ajay
 * @author prasad memane
 * @author swapnil mahajan
 */
public class PredictionReducer extends Reducer<Text, PredictionModel, Text, Text> {

	private static final String TEST = "test";

	@Override
	protected void reduce(Text key, Iterable<PredictionModel> values, Context context)
			throws IOException, InterruptedException {
		ArrayList<Attribute> attributes = Weka.getAttrs();
		Instances test = new Instances(TEST, attributes, 0);
		test.setClassIndex(Weka.MISSED_FEATURE_INDEX);

		try {
			Classifier cls = Utils.readFile(key.toString());
			for (PredictionModel row : values) {
				classify(test, row, cls, context, key.toString());
			}
		} catch (Exception e1) {
			System.err.println("error occoured while classifying" + e1.getCause());
		}
	}

	/**
	 * Predicts each instance of the connection formed from test data using the
	 * model built from connections training data. Prediction indicates if a
	 * connecting flight is missed or not.
	 *
	 * @param testQ
	 * @param row
	 * @param cls
	 * @param context
	 * @param connection
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws Exception
	 */
	private void classify(Instances testQ, PredictionModel row, Classifier cls, Context context, String connection)
			throws IOException, ClassNotFoundException, Exception {
		String missed = "true";

		Instance inst = buildInstance(testQ, row);
		double clsLabel = cls.classifyInstance(inst);
		if (clsLabel == 0.0) missed = "false";

		context.write(new Text(row.getCarrier()),
				new Text(row.getOrigin() + "," + connection + "," + row.getDest() + "," + row.getYear() + ","
						+ row.getMonth() + "," + row.getDay() + "," + row.getDuration() + "," + missed + ","
						+ row.getOrgConnect() + "," + row.getConnectDest()));
	}

	/**
	 * Builds the test Instance used by weka to predict.
	 *
	 * @param testQ
	 * @param row
	 * @return
	 */
	private Instance buildInstance(Instances testQ, Model row) {
		Instance inst = new DenseInstance(Weka.NUM_FEATURES);

		inst.setDataset(testQ);

		inst.setValue(0, Utils.getCarrier(row.getCarrier().toString()));
		inst.setValue(1, Utils.getAirports(row.getOrigin().toString()));
		inst.setValue(2, Utils.getAirports(row.getDest().toString()));
		inst.setValue(3, row.getMonth().get());
		inst.setValue(4, row.getDay().get());
		inst.setValue(5, row.getArr().get());
		inst.setValue(6, row.getDep().get());
		inst.setValue(7, row.getDuration().get());

		return inst;
	}

}
