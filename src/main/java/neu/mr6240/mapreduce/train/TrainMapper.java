package neu.mr6240.mapreduce.train;

import static neu.mr6240.utils.Constants.ARR_TIME;
import static neu.mr6240.utils.Constants.CANCELLED;
import static neu.mr6240.utils.Constants.CARRIER;
import static neu.mr6240.utils.Constants.CRS_ARR_TIME;
import static neu.mr6240.utils.Constants.CRS_DEP_TIME;
import static neu.mr6240.utils.Constants.CRS_ELAPSED_TIME;
import static neu.mr6240.utils.Constants.DAY_OF_MONTH;
import static neu.mr6240.utils.Constants.DEFAULT_ROLL_OVER_FLAG;
import static neu.mr6240.utils.Constants.DEP_TIME;
import static neu.mr6240.utils.Constants.DESTINATION;
import static neu.mr6240.utils.Constants.DESTINATION_INDEX;
import static neu.mr6240.utils.Constants.HISTORY_COLUMNS;
import static neu.mr6240.utils.Constants.MONTH;
import static neu.mr6240.utils.Constants.ORIGIN;
import static neu.mr6240.utils.Constants.ORIGIN_INDEX;
import static neu.mr6240.utils.Constants.YEAR;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import au.com.bytecode.opencsv.CSVParser;
import neu.mr6240.utils.AirRecordSanity;
import neu.mr6240.utils.CarrierOrgDep;
import neu.mr6240.utils.CompositeValue;
import neu.mr6240.utils.Utils;

/**
 * This is the Mapper class for reading the history data
 *
 * @author prasad memane
 * @author swapnil mahajan
 * @info - initial version for a5-paths
 * @author smitha bangalore
 * @author ajay subramanya
 * @info - updated version for a7-routing, added new fields for city and
 *       elapsedTime
 */
public class TrainMapper extends Mapper<LongWritable, Text, CarrierOrgDep, CompositeValue> {

	private CSVParser parser = new CSVParser();
	private AirRecordSanity sanity = new AirRecordSanity();

	/**
	 * This method maps each row to a specific <Key,Value> which is the input to
	 * the reducer depending on the key It reads the input line by line and
	 * throws away the unnecessary information while generating the <Key, Value>
	 * pair
	 */
	@Override
	protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		String[] fields = parser.parseLine(value.toString());

		if (value.toString().contains("YEAR")) return;
		if (fields.length == HISTORY_COLUMNS && !sanity.sanityFails(fields)) {

			boolean rollOverDate = true;
			if (Utils.checkDayRollOver(fields[YEAR], fields[MONTH], fields[DAY_OF_MONTH], fields[CRS_ARR_TIME],
					fields[CRS_DEP_TIME], fields[CRS_ELAPSED_TIME]))
				rollOverDate = false;

			context.write(new CarrierOrgDep(fields[CARRIER], fields[ORIGIN]),
					new CompositeValue(fields[YEAR], fields[MONTH], fields[DAY_OF_MONTH], fields[CRS_DEP_TIME],
							fields[DEP_TIME], fields[CANCELLED], ORIGIN_INDEX, rollOverDate, fields[DESTINATION],
							fields[CRS_ELAPSED_TIME]));

			context.write(new CarrierOrgDep(fields[CARRIER], fields[DESTINATION]),
					new CompositeValue(fields[YEAR], fields[MONTH], fields[DAY_OF_MONTH], fields[CRS_ARR_TIME],
							fields[ARR_TIME], fields[CANCELLED], DESTINATION_INDEX, DEFAULT_ROLL_OVER_FLAG,
							fields[ORIGIN], fields[CRS_ELAPSED_TIME]));
		}
	}
}
