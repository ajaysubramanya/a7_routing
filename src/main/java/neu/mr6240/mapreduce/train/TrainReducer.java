package neu.mr6240.mapreduce.train;

import static neu.mr6240.utils.Constants.CANCELLED_CODE;
import static neu.mr6240.utils.Constants.DESTINATION_INDEX;
import static neu.mr6240.utils.Constants.ONE_HOUR;
import static neu.mr6240.utils.Constants.ONE_HOUR_IN_MINUTES;
import static neu.mr6240.utils.Constants.THIRTY_MINUTES;
import static neu.mr6240.utils.Constants._0_HOURS;
import static neu.mr6240.utils.Constants._24_HOURS;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.Minutes;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;

import neu.mr6240.utils.CarrierOrgDep;
import neu.mr6240.utils.CompositeValue;
import neu.mr6240.utils.MapValue;

/**
 * This is a reducer class which finds connections and missed connections for
 * all years that are present, for the given location, which could be both
 * origin and destination. It outputs the connection along with missed info
 * which is later used for building the model.
 *
 * @author prasad memane
 * @author swapnil mahajan
 * @author ajay subramanya
 * @author smitha bangalore naresh
 */
public class TrainReducer extends Reducer<CarrierOrgDep, CompositeValue, Text, Text> {

	@Override
	protected void reduce(CarrierOrgDep key, Iterable<CompositeValue> values, Context context)
			throws IOException, InterruptedException {

		List<MapValue> arrFlights = new ArrayList<>();
		Map<LocalDate, ListMultimap<Integer, MapValue>> depFlights = new HashMap<>();

		for (CompositeValue cv : values) {
			splitFlights(arrFlights, depFlights, cv);
		}

		connect(arrFlights, depFlights, key, context);
	}

	/**
	 * Adds to arrFlights or depFlights based on the index value sent from
	 * Mapper
	 *
	 * @param arrFlights
	 * @param depFlights
	 * @param cv
	 */
	private void splitFlights(List<MapValue> arrFlights, Map<LocalDate, ListMultimap<Integer, MapValue>> depFlights,
			CompositeValue cv) {
		if (cv.getIndex().get() == DESTINATION_INDEX)
			arrFlights.add(getMapValue(cv));
		else
			addToDep(depFlights, cv);
	}

	/**
	 * Forms connections and missed connections
	 *
	 * @param arrFlights
	 * @param depFlights
	 * @param key
	 * @param context
	 * @return
	 * @throws InterruptedException
	 * @throws IOException
	 */
	private void connect(List<MapValue> arrFlights, Map<LocalDate, ListMultimap<Integer, MapValue>> depFlights,
			CarrierOrgDep key, Reducer<CarrierOrgDep, CompositeValue, Text, Text>.Context context)
					throws IOException, InterruptedException {

		for (MapValue arr : arrFlights) {
			LocalDate date = arr.getCrsDateTime().toLocalDate();

			if (depFlights.containsKey(date)) {
				getDepFlights(depFlights, arr, date, key, context);
			}

			if (noRollOver(arr, date)) continue;
			LocalDate nextDate = date.plusDays(1);

			if (depFlights.containsKey(nextDate)) {
				getDepFlightsNextDay(depFlights, arr, date, key, context);
			}
		}

	}

	/**
	 * Gets all the departing flights within given duration window for an
	 * arrival flight
	 *
	 * @param depFlights
	 * @param arr
	 * @param date
	 * @param key
	 * @param context
	 * @throws InterruptedException
	 * @throws IOException
	 */
	private void getDepFlights(Map<LocalDate, ListMultimap<Integer, MapValue>> depFlights, MapValue arr, LocalDate date,
			CarrierOrgDep key, Reducer<CarrierOrgDep, CompositeValue, Text, Text>.Context context)
					throws IOException, InterruptedException {

		ListMultimap<Integer, MapValue> lm = depFlights.get(date);
		List<MapValue> departures = new ArrayList<>();
		LocalDateTime crsDateTime = arr.getCrsDateTime();

		int start = crsDateTime.getHourOfDay();
		int end = start + ONE_HOUR;

		for (Integer hr : lm.keySet()) {
			if (hr >= start && hr <= end) {
				for (MapValue mv : lm.get(hr))
					departures.add(mv);
			}
		}
		addIfConnection(departures, arr, key, context);
	}

	/**
	 * Gets all the departing flights next day for remaining duration window for
	 * an arrival flight
	 *
	 * @param depFlights
	 * @param arr
	 * @param date
	 * @param key
	 * @param context
	 * @throws InterruptedException
	 * @throws IOException
	 */
	private void getDepFlightsNextDay(Map<LocalDate, ListMultimap<Integer, MapValue>> depFlights, MapValue arr,
			LocalDate date, CarrierOrgDep key, Reducer<CarrierOrgDep, CompositeValue, Text, Text>.Context context)
					throws IOException, InterruptedException {

		ListMultimap<Integer, MapValue> lm = depFlights.get(date);
		if (lm == null) return;
		List<MapValue> departures = new ArrayList<>();
		LocalDateTime crsDateTime = arr.getCrsDateTime();

		int start = _0_HOURS;
		int end = (crsDateTime.getHourOfDay() + ONE_HOUR) % _24_HOURS;

		for (Integer hr : lm.keySet()) {
			if (hr >= start && hr <= end) {
				for (MapValue mv : lm.get(hr))
					departures.add(mv);
			}

		}
		addIfConnection(departures, arr, key, context);
	}

	/**
	 * This method checks for a connection and if present checks if it was
	 * missed and outputs the data
	 *
	 * @param depFlights
	 * @param arrFlight
	 * @param key
	 * @param context
	 * @throws InterruptedException
	 * @throws IOException
	 */
	private void addIfConnection(List<MapValue> depFlights, MapValue arrFlight, CarrierOrgDep key,
			Reducer<CarrierOrgDep, CompositeValue, Text, Text>.Context context)
					throws IOException, InterruptedException {

		boolean isMissed = false;
		for (MapValue dep : depFlights) {
			isMissed = false;
			int crsDur = Minutes.minutesBetween(arrFlight.getCrsDateTime(), dep.getCrsDateTime()).getMinutes();

			if (crsDur < THIRTY_MINUTES || crsDur > ONE_HOUR_IN_MINUTES) {
				continue;
			}

			if (arrFlight.getCancelled() == CANCELLED_CODE
					|| (Minutes.minutesBetween(arrFlight.getActualDateTime(), dep.getActualDateTime())
							.getMinutes() < THIRTY_MINUTES)) {
				isMissed = true;
			}

			/**
			 * write the connection information
			 */
			String startCity = arrFlight.getCity();
			String destCity = dep.getCity();

			short year = (short) arrFlight.getCrsDateTime().getYear();
			short month = (short) arrFlight.getCrsDateTime().getMonthOfYear();
			short day = (short) arrFlight.getCrsDateTime().getDayOfMonth();
			short arrivalHr = (short) arrFlight.getCrsDateTime().getHourOfDay();
			short deptHr = (short) dep.getCrsDateTime().getHourOfDay();

			long duration = arrFlight.getElapsedTime() + crsDur + dep.getElapsedTime();

			context.write(new Text(key.getCarrier()),
					new Text(startCity + "," + key.getOrgDep() + "," + destCity + "," + year + "," + month + "," + day
							+ "," + arrivalHr + "," + deptHr + "," + duration + "," + isMissed));

		}
	}

	/**
	 * This method checks if we have to consider the next day as well for
	 * finding the connecting flights Example: CRSDepartureTime + 6 hours
	 * changes the date ,i.e., any flight after 1800
	 *
	 * @param arr
	 * @param date
	 * @param originMap
	 * @return
	 */
	private boolean noRollOver(MapValue arr, LocalDate date) {
		LocalDateTime crsDateTime = arr.getCrsDateTime().plusHours(ONE_HOUR);
		return crsDateTime.toLocalDate().equals(date);
	}

	/**
	 * This method adds the MapValue object to the depList which contains all
	 * the entries for index set to 0 ,i.e., the CompositeValue object contains
	 * the values related to origin
	 *
	 * @param depFlights
	 * @param cv
	 */
	private void addToDep(Map<LocalDate, ListMultimap<Integer, MapValue>> depFlights, CompositeValue cv) {

		LocalDate date = new LocalDate(cv.getYear().get(), cv.getMonth().get(), cv.getDayOfMonth().get());
		Integer depTime = Integer.parseInt(cv.getCrsTime().toString()) / 100;
		if (depFlights.containsKey(date)) {
			depFlights.get(date).put(depTime, getMapValue(cv));
		} else {
			ListMultimap<Integer, MapValue> mm = ArrayListMultimap.create();
			mm.put(depTime, getMapValue(cv));
			depFlights.put(date, mm);
		}
	}

	/**
	 * This method returns the MapValue based on the rollOver field set in the
	 * CompositeValue outputted as value by the mapper
	 *
	 * @param cv
	 * @return
	 */
	private MapValue getMapValue(CompositeValue cv) {
		MapValue mv = null;
		if (cv.getRollOver().get()) {
			mv = new MapValue(cv.getYear().get(), cv.getMonth().get(), cv.getDayOfMonth().get(),
					cv.getCrsTime().toString(), cv.getTime().toString(), cv.getCancelled().get(), true,
					cv.getCity().toString(), cv.getElapsedTime().get());
		} else {
			mv = new MapValue(cv.getYear().get(), cv.getMonth().get(), cv.getDayOfMonth().get(),
					cv.getCrsTime().toString(), cv.getTime().toString(), cv.getCancelled().get(),
					cv.getCity().toString(), cv.getElapsedTime().get());
		}

		return mv;
	}
}