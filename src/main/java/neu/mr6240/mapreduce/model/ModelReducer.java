package neu.mr6240.mapreduce.model;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import neu.mr6240.utils.Utils;
import neu.mr6240.utils.mapreduce.TrainingModel;
import neu.mr6240.utils.weka.Weka;
import weka.classifiers.bayes.NaiveBayes;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

/**
 * This is the Reducer class for building a model. TrainingModel contains
 * Connections data along with missed info for a flight. Naive Bayes is used for
 * building a model for a airport(i.e. connecting airport)
 *
 * @author smitha
 * @author ajay
 * @author prasadmemane
 * @author swapnilmahajan
 */

public class ModelReducer extends Reducer<Text, TrainingModel, Text, Text> {

	private static final String TRAINING = "training";

	@Override
	/**
	 * @param key
	 *            connecting airport
	 * @param values
	 *            list of flights for the connecting airport
	 * 
	 */
	protected void reduce(Text key, Iterable<TrainingModel> values,
			Reducer<Text, TrainingModel, Text, Text>.Context context) throws IOException, InterruptedException {
		ArrayList<Attribute> attributes = Weka.getAttrs();
		Instances training = new Instances(TRAINING, attributes, 0);
		training.setClassIndex(Weka.MISSED_FEATURE_INDEX);

		for (TrainingModel row : values) {
			buildDataSet(training, row);
		}
		this.buildModel(training, key.toString());
	}

	/**
	 *
	 * @param training
	 *            using which the model will be built
	 * @param connect
	 *            connecting airport
	 */
	private void buildModel(Instances training, String connect) {

		NaiveBayes naive = new NaiveBayes();
		try {
			naive.buildClassifier(training);
			Utils.writeFile(naive, connect);
		} catch (Exception e) {
			new Error(e.getCause());
		}
	}

	/**
	 *
	 * @param training
	 *            meta-data about the columns
	 * @param row
	 *            the row which will be added to the dataset
	 */
	private void buildDataSet(Instances training, TrainingModel row) {
		Instance inst = new DenseInstance(Weka.NUM_FEATURES);
		inst.setDataset(training);

		inst.setValue(0, Utils.getCarrier(row.getCarrier().toString()));
		inst.setValue(1, Utils.getAirports(row.getOrigin().toString()));
		inst.setValue(2, Utils.getAirports(row.getDest().toString()));
		inst.setValue(3, row.getMonth().get());
		inst.setValue(4, row.getDay().get());
		inst.setValue(5, row.getArr().get());
		inst.setValue(6, row.getDep().get());
		inst.setValue(7, row.getDuration().get());
		inst.setValue(8, row.getMissed().toString());

		training.add(inst);
	}
}
