package neu.mr6240.mapreduce.model;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import au.com.bytecode.opencsv.CSVParser;
import neu.mr6240.utils.mapreduce.TrainingIndex;
import neu.mr6240.utils.mapreduce.TrainingModel;

/**
 * This is the Mapper class for reading the connections data for building model.
 * Connections data contains missed info for a flight
 *
 * @author smitha
 * @author ajay
 * @author prasadmemane
 * @author swapnilmahajan
 */

public class ModelMapper extends Mapper<LongWritable, Text, Text, TrainingModel> {

	private static final String _2004 = "2004";
	private CSVParser parser = new CSVParser();

	@Override
	/**
	 * @param key
	 * @param value
	 *            a line from the output of training
	 * 
	 * @emits (connecting_airport, TrainingModel)
	 */
	protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, TrainingModel>.Context context)
			throws IOException, InterruptedException {
		String[] fields = parser.parseLine(value.toString());
		if (fields[TrainingIndex.YEAR.val()].equals(_2004)) return;
		context.write(new Text(fields[TrainingIndex.CONN.val()]), TrainingModel.set(fields));
	}
}
